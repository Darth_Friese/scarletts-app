//
//  mathModel.swift
//  Scalett's App
//
//  Created by Jeremy Friese on 7/5/19.
//  Copyright © 2019 Jeremy Friese. All rights reserved.
//

//import Foundation
import UIKit

class mathModel {
    
    var numberImageOne: String = ""
    var numberImageTwo: String = ""
    
    func getNumberPic(num: Int) -> String {
        switch(num) {
        case 1:
            return("one")
        case 2:
            return("two")
        case 3:
            return("three")
        case 4:
            return("four")
        case 5:
            return("five")
        case 6:
            return("six")
        case 7:
            return("seven")
        case 8:
            return("eight")
        case 9:
            return("nine")
        case 0:
            return("zero")
        default:
            return("This is not the number you are looking for")
        }
    }
}
