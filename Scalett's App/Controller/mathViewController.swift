//
//  ViewController.swift
//  Scalett's App
//
//  Created by Jeremy Friese on 6/28/19.
//  Copyright © 2019 Jeremy Friese. All rights reserved.
//

import UIKit
import AVFoundation

var exp = 0

class mathViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background.png")!)
            updateNums()
    }
    
    public var num1 = Int(1)
    public var num2 = Int(2)
    let MathModel = mathModel()
    
    func updateNums() {
        num1 = Int.random(in: 1 ... 9)
        let imageOne = MathModel.getNumberPic(num: num1)
        number1.image = UIImage(named: imageOne)
        num2 = Int.random(in: 1 ... 9)
        let imageTwo = MathModel.getNumberPic(num: num2)
        number2.image = UIImage(named: imageTwo)
        plusMinusTimes.image = UIImage(named: "plus")
        underLine.image = UIImage(named: "line")
        
        speakQuestion()
    }
        
    @IBOutlet weak var number2: UIImageView!
    @IBOutlet weak var number1: UIImageView!
    @IBOutlet weak var mathAnswer: UITextField!
    @IBOutlet weak var plusMinusTimes: UIImageView!
    @IBOutlet weak var underLine: UIImageView!
    
    
    @IBAction func checkAnswer(_ sender: UIButton) {
            guard let mathA = Int(mathAnswer.text!) else { return _ = Int(num1) }
            compareAnswer(inputAnswer: mathA)
        }
        
        func compareAnswer(inputAnswer : Int) {
            let correctAnswer = num1 * num2
            if correctAnswer == inputAnswer {
                let answers = "Correct"
                exp = exp + 1
                print(exp)
                speakResponse(answers: answers)
                sleep(2)
                //speakResponse(answers: String(exp))
                self.mathAnswer.text = ""
                updateNums()
            }
            else {
                let answers = "Incorrect." + String(num1) + "times" + String(num2) + "is" + String(correctAnswer)
                speakResponse(answers: answers)
                sleep(4)
                self.mathAnswer.text = ""
                updateNums()
            }
        }
        
        func speakResponse(answers : String) {
            let utterance = AVSpeechUtterance(string: answers)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            utterance.rate = 0.5
            
            let synthesizer = AVSpeechSynthesizer()
            synthesizer.speak(utterance)
        }
        
        func speakQuestion(){
            let number1 = String(num1)
            let number2 = String(num2)
            let utterance = AVSpeechUtterance(string: "What is " + number1 + " times" + number2)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            utterance.rate = 0.5
            
            let synthesizer = AVSpeechSynthesizer()
            synthesizer.speak(utterance)
        }
}
